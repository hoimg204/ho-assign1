//
//  main.m
//  ho-assign1
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-17.
//  Copyright (c) 2014 beta. All rights reserved.
//

// Problem Statement:
// Write an application that prints the following diamond shape.
// Don’t print any unneeded characters. (That is, don’t make any
// character string longer than it has to be.)
//
//    *
//   ***
//  *****
// *******
//*********
// *******
//  *****
//   ***
//    *
//
//Inputs:   none
//Outputs:  requested diamond shape using * character

// Problem Statement:
// Write an application that displays your initials in large block letters.
// Make each large letter out of the corresponding regular character. Each
// line contains a number of characters in its line. The number must be
// properly formatted and aligned.
// For example:
//
//  FFFFFFFFFFFFFFFF    NN             NN    18
//  FFFFFFFFFFFFFFFF    NNN            NN    19
//  FF                  NNNN           NN     8
//  FF                  NN NN          NN     8
//  FF                  NN  NN         NN     8
//  FF                  NN   NN        NN     8
//  FFFFFFFF            NN    NN       NN    14
//  FFFFFFFF            NN     NN      NN    14
//  FF                  NN      NN     NN     8
//  FF                  NN       NN    NN     8
//  FF                  NN        NN   NN     8
//  FF                  NN         NN  NN     8
//  FF                  NN          NN NN     8
//  FF                  NN           NNNN     8
//  FF                  NN            NNN     7
//
// Inputs:   none
// Outputs:  requested initials in large block characters

#import <Foundation/Foundation.h>

int main(int argc, const char *argv[]) {
	@autoreleasepool {
		//First Problem
		printf("    *\n");
		printf("   ***\n");
		printf("  *****\n");
		printf(" *******\n");
		printf("*********\n");
		printf(" *******\n");
		printf("  *****\n");
		printf("   ***\n");
		printf("    *\n\n");

		//Second Problem
		NSString *myName[6];
		//sizeArray is a variable that has the size of array
		int sizeArray = (sizeof myName) / (sizeof myName[0]);

		//my name in Large Letter
		myName[0] = @"HH  HH  OOOOOO";
		myName[1] = @"HH  HH  OO  OO";
		myName[2] = @"HHHHHH  OO  OO";
		myName[3] = @"HHHHHH  OO  OO";
		myName[4] = @"HH  HH  OO  OO";
		myName[5] = @"HH  HH  OOOOOO";

		for (int count = 0; count < sizeArray; count++) {
			//Remove all whitespace
			NSArray *words = [myName[count] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			NSString *noSpaceString = [words componentsJoinedByString:@""];
			NSLog(@"%@ %lu", myName[count], (unsigned long)noSpaceString.length);
		}
	}
	return 0;
}
